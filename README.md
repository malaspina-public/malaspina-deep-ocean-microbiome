# _Deep ocean metagenomes provide insight into the metabolic architecture of bathypelagic microbial communities_ Companion website.

This is a repo to store and make vailable all supplementary material to the manuscript

**Deep ocean metagenomes provide insight into the metabolic architecture of bathypelagic microbial communities.** Silvia G. Acinas, Pablo Sánchez, Guillem Salazar, Francisco M. Cornejo-Castillo, Marta Sebastián, Ramiro Logares, Marta Royo-Llonch, Lucas Paoli, Shinichi Sunagawa, Pascal Hingamp, Hiroyuki Ogata, Gipsi Lima-Mendez, Simon Roux, José M. González, Jesús M. Arrieta, Intikhab S. Alam,  Allan Kamau, Chris Bowler, Jeroen Raes, Stéphane Pesant, Peer Bork, Susana Agustí, Takashi Gojobori, Dolors Vaqué, Matthew B. Sullivan, Carlos Pedrós-Alió, Ramon Massana, Carlos M. Duarte, Josep M. Gasol.
