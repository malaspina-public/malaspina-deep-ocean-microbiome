---
title: Table Of Contents
date: 2021-04-28
---

Additional meterial for the manuscript can be found in the [Data](https://malaspina-public.gitlab.io/malaspina-deep-ocean-microbiome/page/data) section:  

[**Companion website data**](https://malaspina-public.gitlab.io/malaspina-deep-ocean-microbiome/page/data/)  
- Malaspina Gene Database (M-geneDB)  
- Functional tables  
    - Raw Functional tables  
    - Subsampled Functional tables  
- Taxonomic tables  
- Metagenome Assembled Genomes (MAGs)  
- Manuscript suuplementary tables (data availability)  
- Manuscript supplementary information  
- Manuscript supplementary figures

