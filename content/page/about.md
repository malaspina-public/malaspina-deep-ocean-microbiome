---
title: Deep ocean metagenomes provide insight into the metabolic architecture of bathypelagic microbial communities
subtitle: Companion Website
comments: false
---

A companion website that holds additional resources.
